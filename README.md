
<!--![Screenshot1](./screens/screen01.png)
![Screenshot2](./screens/screen02.png)
![Screenshot3](./screens/screen03.png)
![Screenshot3](./screens/screen04.png)
![Screenshot5](./screens/screen05.png)-->

<p align="center">
  <img src="./screens/screen01.png" width="200" />
  <img src="./screens/screen02.png" width="200" />
  <img src="./screens/screen03.png" width="200" />
</p>
<p align="center">
  <img src="./screens/screen04.png" width="200" />
  <img src="./screens/screen05.png" width="200" />
</p>

## Para executar o APP

Primeiro execute o comando `flutter pub get` na raiz do projeto e em seguida execute `flutter pub run build_runner build --delete-conflicting-outputs`.
