class Rates {

  final oneHundred = 100;

  multiplyPerOneHundred(double value) {
    return value * oneHundred;
  }

  double calcRate(double rateCompetitor, double minimumTaxe) {
    return (rateCompetitor * (1 - minimumTaxe / oneHundred));
  }

  static final Rates instance = Rates();
}