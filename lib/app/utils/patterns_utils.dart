import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class PatternsUtils {
  static final phone = new MaskTextInputFormatter(mask: '(##) #####-####', filter: { "#": RegExp(r'[0-9]') });
  static final cpf = new MaskTextInputFormatter(mask: '###.###.###-##', filter: { "#": RegExp(r'[0-9]') });
}