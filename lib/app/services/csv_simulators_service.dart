import 'dart:io';

import 'package:csv/csv.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simulator_credit_card/app/constants/status_simulator_constant.dart';
import 'package:simulator_credit_card/app/interfaces/simulator_repository_interface.dart';
import 'package:simulator_credit_card/app/interfaces/csv_simulators_interface.dart';
import 'package:simulator_credit_card/app/utils/rates.dart';

class CsvSimulatorsService implements ICsvSimulators {
  final ISimulatorRepository simulatorRepository;

  CsvSimulatorsService({@required this.simulatorRepository});

  @override
  Future<void> generateCSVSimulatorAcceptAndOpen() async {
    var simulations =
        await this.simulatorRepository.findByStatus(StatusSimulator.accept);

    List<List<dynamic>> csvData = [
      <String>[
        'Concorrente',
        'CPF cliente',
        'Telefone cliente',
        'Email cliente',
        'Ramo de atividade',
        'Débito concorrente',
        'Crédito concorrente',
        'Taxa de débito aceita',
        'Taxa de crédito aceita',
        'Date de aceite'
      ],
      ...simulations.map((simulator) => [
            simulator?.competitor?.name,
            simulator?.client?.cpf,
            simulator.client?.phone,
            simulator?.client?.email,
            simulator?.client?.branch?.name,
            '${simulator?.debitRate?.rateCompetitor?.toStringAsFixed(2)}%',
            '${simulator?.creditRate?.rateCompetitor?.toStringAsFixed(2)}%',
            '${Rates.instance.calcRate(simulator?.debitRate?.rateCompetitor, simulator?.debitRate?.minimumDiscount)}%',
            '${Rates.instance.calcRate(simulator?.creditRate?.rateCompetitor, simulator?.creditRate?.minimumDiscount)}%',
            '${simulator?.createdAt?.day}/${simulator?.createdAt?.month}/${simulator?.createdAt?.year}'
          ]),
    ];

    String csv = ListToCsvConverter().convert(csvData);

    final String documentDirectory =
        (await getApplicationDocumentsDirectory()).path;
    final String path = '$documentDirectory/simulators_accept.csv';
    final File file = File(path);

    await file.writeAsString(csv);

    await OpenFile.open(file.path);
  }

}
