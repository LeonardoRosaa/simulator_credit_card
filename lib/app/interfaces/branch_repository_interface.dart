import 'package:simulator_credit_card/app/models/branch_model.dart';

abstract class IBranchRepository {

  Future<List<BranchModel>> findAll();
}