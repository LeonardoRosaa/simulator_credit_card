import 'package:simulator_credit_card/app/models/simulator_data_model.dart';

abstract class ISimulatorRepository {

  Future<String> create(SimulatorDataModel simulator);
  Future<List<SimulatorDataModel>> findByStatus(String status);
  
}