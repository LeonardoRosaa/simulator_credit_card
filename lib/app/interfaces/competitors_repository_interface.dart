import 'package:simulator_credit_card/app/models/competitor_model.dart';

abstract class ICompetitorsRepository {

  Future<List<CompetitorModel>> findAll();

}