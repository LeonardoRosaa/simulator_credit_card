import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InputWidget extends StatelessWidget {
  
  final String hintText;
  final onChange;
  final inputFormatters;
  final Function(String) validator;
  final TextInputType textInputType;

  InputWidget({
    this.hintText, 
    this.onChange, 
    this.inputFormatters, 
    this.validator,
    this.textInputType = TextInputType.text
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChange,
      inputFormatters: inputFormatters,
      validator: validator,
      
      keyboardType: textInputType,
      decoration: InputDecoration(
        filled: true,
        hintText: this.hintText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            width: 0, color: Colors.transparent
          )
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            width: 1, color: Color.fromRGBO(44, 44, 220, 1)
          )
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            width: 0, color: Colors.transparent)
        )
      ),
    );
  }
}