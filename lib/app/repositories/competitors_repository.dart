import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:simulator_credit_card/app/interfaces/competitors_repository_interface.dart';
import 'package:simulator_credit_card/app/models/competitor_model.dart';

class CompetitorsRepository implements ICompetitorsRepository {

  final Firestore firestore;

  CompetitorsRepository({@required this.firestore});

  CollectionReference get dbRef => Firestore.instance.collection('competitors');

  @override
  Future<List<CompetitorModel>> findAll() async {
    var snapshot = await dbRef.orderBy('name').getDocuments();
    
    List<CompetitorModel> competitors = snapshot.documents
      .map((e) => CompetitorModel.fromJson(e.data))
      .toList();

    return competitors;
  }
  
}