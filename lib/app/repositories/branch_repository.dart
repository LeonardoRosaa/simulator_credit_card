import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:simulator_credit_card/app/interfaces/branch_repository_interface.dart';
import 'package:simulator_credit_card/app/models/branch_model.dart';

class BranchRepository implements IBranchRepository {
  final Firestore firestore;

  BranchRepository({@required this.firestore});

  CollectionReference get dbRef => Firestore.instance.collection('branches');

  @override
  Future<List<BranchModel>> findAll() async {
    var snapshot = await dbRef.orderBy('name').getDocuments();
    
    List<BranchModel> branches = snapshot.documents
      .map((e) => BranchModel.fromJson(e.data))
      .toList();

    return branches;
  }
}
