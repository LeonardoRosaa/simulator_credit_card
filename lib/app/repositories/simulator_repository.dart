import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:simulator_credit_card/app/interfaces/simulator_repository_interface.dart';
import 'package:simulator_credit_card/app/models/simulator_data_model.dart';

class SimulatorRepository implements ISimulatorRepository {
  final Firestore firestore;

  SimulatorRepository({@required this.firestore});

  CollectionReference get dbRef => Firestore.instance.collection('simulations');

  @override
  Future<String> create(SimulatorDataModel simulator) async {
    simulator.id = dbRef.document().documentID;

    simulator.createdAt = DateTime.now();
    
    await dbRef.document(simulator.id).setData(simulator.toJson());

    return simulator.id;
  }

  @override
  Future<List<SimulatorDataModel>> findByStatus(String status) async {
    var snapshots =
        await dbRef.where('status', isEqualTo: status).getDocuments();

    List<SimulatorDataModel> simulations =  snapshots.documents
        .map((simulator) => SimulatorDataModel.fromJson(simulator.data))
        .toList();

    return simulations;   
  }
}
