import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: Modular.navigatorKey,
      title: 'Flutter Slidy',
      theme: ThemeData(
        scaffoldBackgroundColor: Color.fromRGBO(245, 246, 252, 1),
        appBarTheme: AppBarTheme(
          elevation: 0,
          color: Color.fromRGBO(157, 173, 245, 1)
        )
      ),
      initialRoute: '/simulator',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: Modular.generateRoute,
    );
  }
}
