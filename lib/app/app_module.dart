import 'package:simulator_credit_card/app/screens/simulator/create_simulation/create_simulation_module.dart';
import 'package:simulator_credit_card/app/screens/simulator/home_simulator/home_simulator_module.dart';

import 'app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:simulator_credit_card/app/app_widget.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
      ];

  @override
  List<Router> get routers => [
        Router('/simulator', module: HomeSimulatorModule()),
        Router('/simulator/store/client', module: CreateSimulationModule())
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
