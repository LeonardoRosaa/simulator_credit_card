import 'package:json_annotation/json_annotation.dart';

part 'branch_model.g.dart';

@JsonSerializable()
class BranchModel {
  String id;
  String name;
  double minimumDebit;
  double minimumCredit;

  BranchModel({this.name, this.id, this.minimumCredit, this.minimumDebit});

  factory BranchModel.fromJson(Map<String, dynamic> json) =>
      _$BranchModelFromJson(json);

  Map<String, dynamic> toJson() => _$BranchModelToJson(this);
}
