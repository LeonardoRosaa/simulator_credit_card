import 'package:json_annotation/json_annotation.dart';

part 'rate_model.g.dart';

@JsonSerializable()
class RateModel {
  
  double rateCompetitor;
  double minimumDiscount;

  RateModel({this.rateCompetitor, this.minimumDiscount});

  factory RateModel.fromJson(Map<String, dynamic> json) =>
      _$RateModelFromJson(json);

  Map<String, dynamic> toJson() => _$RateModelToJson(this);
}
