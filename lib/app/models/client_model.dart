import 'package:json_annotation/json_annotation.dart';
import 'package:simulator_credit_card/app/models/branch_model.dart';

part 'client_model.g.dart';

@JsonSerializable()
class ClientModel {
  
  String cpf;
  String phone;
  String email;
  BranchModel branch;

  ClientModel({this.cpf, this.phone, this.email});

  factory ClientModel.fromJson(Map<String, dynamic> json) =>
      _$ClientModelFromJson(json);

  Map<String, dynamic> toJson() => _$ClientModelToJson(this);
}
