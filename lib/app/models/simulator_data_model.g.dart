// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'simulator_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SimulatorDataModel _$SimulatorDataModelFromJson(Map<String, dynamic> json) {
  return SimulatorDataModel(
    id: json['id'] as String,
    client: json['client'] == null
        ? null
        : ClientModel.fromJson(json['client'] as Map<String, dynamic>),
    competitor: json['competitor'] == null
        ? null
        : CompetitorModel.fromJson(json['competitor'] as Map<String, dynamic>),
    debitRate: json['debitRate'] == null
        ? null
        : RateModel.fromJson(json['debitRate'] as Map<String, dynamic>),
    creditRate: json['creditRate'] == null
        ? null
        : RateModel.fromJson(json['creditRate'] as Map<String, dynamic>),
    status: json['status'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
  );
}

Map<String, dynamic> _$SimulatorDataModelToJson(SimulatorDataModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'client': instance.client?.toJson(),
      'competitor': instance.competitor?.toJson(),
      'debitRate': instance.debitRate?.toJson(),
      'creditRate': instance.creditRate?.toJson(),
      'status': instance.status,
      'createdAt': instance.createdAt?.toIso8601String(),
    };
