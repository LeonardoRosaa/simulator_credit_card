// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'competitor_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CompetitorModel _$CompetitorModelFromJson(Map<String, dynamic> json) {
  return CompetitorModel(
    id: json['id'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$CompetitorModelToJson(CompetitorModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
