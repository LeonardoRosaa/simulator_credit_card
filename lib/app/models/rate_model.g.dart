// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rate_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RateModel _$RateModelFromJson(Map<String, dynamic> json) {
  return RateModel(
    rateCompetitor: (json['rateCompetitor'] as num)?.toDouble(),
    minimumDiscount: (json['minimumDiscount'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$RateModelToJson(RateModel instance) => <String, dynamic>{
      'rateCompetitor': instance.rateCompetitor,
      'minimumDiscount': instance.minimumDiscount,
    };
