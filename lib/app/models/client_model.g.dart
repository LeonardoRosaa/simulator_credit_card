// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClientModel _$ClientModelFromJson(Map<String, dynamic> json) {
  return ClientModel(
    cpf: json['cpf'] as String,
    phone: json['phone'] as String,
    email: json['email'] as String,
  )..branch = json['branch'] == null
      ? null
      : BranchModel.fromJson(json['branch'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ClientModelToJson(ClientModel instance) =>
    <String, dynamic>{
      'cpf': instance.cpf,
      'phone': instance.phone,
      'email': instance.email,
      'branch': instance.branch?.toJson(),
    };
