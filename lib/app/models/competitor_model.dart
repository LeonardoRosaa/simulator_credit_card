import 'package:json_annotation/json_annotation.dart';

part 'competitor_model.g.dart';

@JsonSerializable()
class CompetitorModel {
  String id;
  String name;

  CompetitorModel({this.id, this.name});

  factory CompetitorModel.fromJson(Map<String, dynamic> json) =>
      _$CompetitorModelFromJson(json);

  Map<String, dynamic> toJson() => _$CompetitorModelToJson(this);
}
