// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'branch_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BranchModel _$BranchModelFromJson(Map<String, dynamic> json) {
  return BranchModel(
    name: json['name'] as String,
    id: json['id'] as String,
    minimumCredit: (json['minimumCredit'] as num)?.toDouble(),
    minimumDebit: (json['minimumDebit'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$BranchModelToJson(BranchModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'minimumDebit': instance.minimumDebit,
      'minimumCredit': instance.minimumCredit,
    };
