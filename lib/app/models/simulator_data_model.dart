import 'package:json_annotation/json_annotation.dart';
import 'package:simulator_credit_card/app/models/client_model.dart';

import 'competitor_model.dart';
import 'rate_model.dart';

part 'simulator_data_model.g.dart';

@JsonSerializable()
class SimulatorDataModel {
  
  String id;
  ClientModel client;
  CompetitorModel competitor;
  RateModel debitRate;
  RateModel creditRate;
  String status;
  DateTime createdAt;

  SimulatorDataModel({
    this.id,
    this.client,
    this.competitor,
    this.debitRate,
    this.creditRate,
    this.status,
    this.createdAt
  });

  factory SimulatorDataModel.fromJson(Map<String, dynamic> json) =>
      _$SimulatorDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$SimulatorDataModelToJson(this);
}
