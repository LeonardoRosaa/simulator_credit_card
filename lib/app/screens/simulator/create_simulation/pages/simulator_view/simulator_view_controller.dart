import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:simulator_credit_card/app/constants/status_simulator_constant.dart';
import 'package:simulator_credit_card/app/interfaces/simulator_repository_interface.dart';
import 'package:simulator_credit_card/app/models/simulator_data_model.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/stores/create_simulator_store.dart';
import 'package:simulator_credit_card/app/utils/rates.dart';

part 'simulator_view_controller.g.dart';

class SimulatorViewController = _SimulatorViewControllerBase
    with _$SimulatorViewController;

abstract class _SimulatorViewControllerBase with Store {
  final oneHundred = 100;
  CreateSimulatorStore _createSimulatorStore;
  ISimulatorRepository _simulatorRepository;

  _SimulatorViewControllerBase(this._createSimulatorStore, this._simulatorRepository);

  @computed
  String get finalRateCredit => calcRateCredit().toStringAsFixed(2);

  @computed
  String get finalRateDebit => calcRateDebit().toStringAsFixed(2);

  @computed 
  String get competitorDebit => _createSimulatorStore.simulator?.debitRate?.rateCompetitor?.toStringAsFixed(2);

  @computed 
  String get competitorCredit => _createSimulatorStore.simulator?.creditRate?.rateCompetitor?.toStringAsFixed(2);

  @computed
  bool get isValidDebit => isAcceptableDebit();

  @computed
  bool get isValidCredit => isAcceptableCredit();

  @computed
  bool get isAcceptableRate => isAcceptableDebit() && isAcceptableCredit();

  @computed
  double get minimumDebit => _createSimulatorStore.simulator.client.branch.minimumDebit * oneHundred;

  @computed
  double get minimumCredit => _createSimulatorStore.simulator.client.branch.minimumCredit * oneHundred;

  @action
  handleAccept() {
    var simulator = _createSimulatorStore.simulator;
    simulator.status = StatusSimulator.accept;
    
    _saveSimulation(simulator);
  }

  @action 
  handleRejected() {
    var simulator = _createSimulatorStore.simulator;
    simulator.status = StatusSimulator.rejected;
    
    _saveSimulation(simulator);
  }

  calcRateDebit() {
    return Rates.instance.calcRate(_createSimulatorStore.simulator.debitRate.rateCompetitor, _createSimulatorStore.simulator.debitRate.minimumDiscount);
  }

  calcRateCredit() {
    return Rates.instance.calcRate(_createSimulatorStore.simulator.creditRate.rateCompetitor, _createSimulatorStore.simulator.creditRate.minimumDiscount);
  }

  isAcceptableDebit() => calcRateDebit() >= minimumDebit;

  isAcceptableCredit() => calcRateCredit() >= minimumCredit;

  Future<void> _saveSimulation(SimulatorDataModel simulator) async {
    try {
      await _simulatorRepository.create(simulator);

      Modular.to.pushNamed('/simulator');
    } catch(error) {
      print(error);
    }
  }

  handleAdjustment() {
    Modular.to.pop();
  }
}
