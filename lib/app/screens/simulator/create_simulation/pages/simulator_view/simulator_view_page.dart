import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/shared/widgets/next_button_widget.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/shared/widgets/text_table_row_widget.dart';
import 'simulator_view_controller.dart';

class SimulatorViewPage extends StatefulWidget {
  final String title;
  const SimulatorViewPage({Key key, this.title = "SimulatorView"})
      : super(key: key);

  @override
  _SimulatorViewPageState createState() => _SimulatorViewPageState();
}

class _SimulatorViewPageState
    extends ModularState<SimulatorViewPage, SimulatorViewController> {

  @override
  Widget build(BuildContext context) {

    void _showDialog() {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))
            ),
            content: Container(
              height: 210,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Salvo com sucesso',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w400
                    ),
                  ),
                  Icon(
                    Icons.done_all,
                    size: 80,
                    color: Colors.greenAccent,
                  ),
                  NextButtonWidget(onTap: () => Modular.to.pop(), text: 'Fechar', icon: Icons.close,)
                ],
              ),
            ),
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Proposta'),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 30),

        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    !controller.isAcceptableRate ? 'Taxas da simulação não permitidas' : 'Taxas da simulação permitidas',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 19,
                    ),
                  ),
                  SizedBox(height: 5,),
                  Text(
                    !controller.isAcceptableRate ? 'A(s) taxas(s) simulada(s) não foi(ram) permitida(s) pois não atinge(m) o mínimo requisitado.' : 'Todas as taxas simuladas são permitidas.',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 30),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                // color: Color.fromRGBO(181, 196, 255, 1),
              ),
              child: Table(
                children: [
                  TableRow(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 1,
                          color: Color.fromRGBO(44, 44, 220, 1),
                        )
                      )
                    ),
                    children: [
                      TextTableRow(
                        text: 'Tipo',
                      ),
                      TextTableRow(
                        text: 'Concorrente',
                      ),
                      TextTableRow(
                        text: 'Proposta',
                      )
                    ]
                  ),
                  TableRow(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 1,
                          color: Color.fromRGBO(44, 44, 220, 1),
                        )
                      )
                    ),
                    children: [
                      TextTableRow(
                        text: 'Débito'
                      ),
                      Observer(
                        builder: (_) => TextTableRow(
                          text: '${controller.competitorDebit}%'
                        ),
                      ),
                      Observer(
                        builder: (_) => TextTableRow(
                          text: '${controller?.finalRateDebit}%',
                          color: controller.isValidDebit ? Colors.greenAccent : Colors.redAccent,
                        ),
                      )
                    ]
                  ),
                  TableRow(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 1,
                          color: Color.fromRGBO(44, 44, 220, 1),
                        )
                      )
                    ),
                    children: [
                      TextTableRow(
                        text: 'Crédito',
                      ),
                      Observer(
                        builder: (_) => TextTableRow(
                          text: '${controller.competitorCredit}%',
                        ),
                      ),
                      Observer(
                        builder: (_) => TextTableRow(
                          text: '${controller?.finalRateCredit}%',
                          color: controller.isValidCredit ? Colors.greenAccent : Colors.redAccent
                        ),
                      )
                    ]
                  )
                ],
              ),
            ),
            Observer(
              builder: (_) => Container(
                child: Column(
                  children: <Widget>[
                    Visibility(
                      visible: !controller.isAcceptableRate,
                      child: Column(
                        children: <Widget>[
                          Text(
                            'O valor de taxa mínima para débito é ${controller.minimumDebit}% enquanto para crédito é ${controller.minimumCredit}%',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(height: 40),
                          NextButtonWidget(
                            onTap: controller.handleAdjustment,
                            text: 'Ajustar', 
                            icon:Icons.arrow_back_ios, 
                          )
                        ],
                      ),
                    ),
                    Visibility(
                      visible: controller.isAcceptableRate,
                      child: Column(
                        children: <Widget>[
                          NextButtonWidget(
                            onTap: ()  {
                              controller.handleAccept();
                              _showDialog();
                            },
                            text: 'Aceitar'),
                          SizedBox(height: 10),
                          NextButtonWidget(
                            onTap: ()  {
                              controller.handleRejected();
                              _showDialog();
                            }, 
                            text: 'Rejeitar', 
                            icon:Icons.arrow_back_ios, 
                            colorsGradient: [
                              Color.fromRGBO(230, 30, 77, 1),
                              Color.fromRGBO(227, 28, 95, 1),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
