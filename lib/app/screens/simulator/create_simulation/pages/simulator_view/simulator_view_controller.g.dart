// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'simulator_view_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SimulatorViewController on _SimulatorViewControllerBase, Store {
  Computed<String> _$finalRateCreditComputed;

  @override
  String get finalRateCredit => (_$finalRateCreditComputed ??= Computed<String>(
          () => super.finalRateCredit,
          name: '_SimulatorViewControllerBase.finalRateCredit'))
      .value;
  Computed<String> _$finalRateDebitComputed;

  @override
  String get finalRateDebit =>
      (_$finalRateDebitComputed ??= Computed<String>(() => super.finalRateDebit,
              name: '_SimulatorViewControllerBase.finalRateDebit'))
          .value;
  Computed<String> _$competitorDebitComputed;

  @override
  String get competitorDebit => (_$competitorDebitComputed ??= Computed<String>(
          () => super.competitorDebit,
          name: '_SimulatorViewControllerBase.competitorDebit'))
      .value;
  Computed<String> _$competitorCreditComputed;

  @override
  String get competitorCredit => (_$competitorCreditComputed ??=
          Computed<String>(() => super.competitorCredit,
              name: '_SimulatorViewControllerBase.competitorCredit'))
      .value;
  Computed<bool> _$isValidDebitComputed;

  @override
  bool get isValidDebit =>
      (_$isValidDebitComputed ??= Computed<bool>(() => super.isValidDebit,
              name: '_SimulatorViewControllerBase.isValidDebit'))
          .value;
  Computed<bool> _$isValidCreditComputed;

  @override
  bool get isValidCredit =>
      (_$isValidCreditComputed ??= Computed<bool>(() => super.isValidCredit,
              name: '_SimulatorViewControllerBase.isValidCredit'))
          .value;
  Computed<bool> _$isAcceptableRateComputed;

  @override
  bool get isAcceptableRate => (_$isAcceptableRateComputed ??= Computed<bool>(
          () => super.isAcceptableRate,
          name: '_SimulatorViewControllerBase.isAcceptableRate'))
      .value;
  Computed<double> _$minimumDebitComputed;

  @override
  double get minimumDebit =>
      (_$minimumDebitComputed ??= Computed<double>(() => super.minimumDebit,
              name: '_SimulatorViewControllerBase.minimumDebit'))
          .value;
  Computed<double> _$minimumCreditComputed;

  @override
  double get minimumCredit =>
      (_$minimumCreditComputed ??= Computed<double>(() => super.minimumCredit,
              name: '_SimulatorViewControllerBase.minimumCredit'))
          .value;

  final _$_SimulatorViewControllerBaseActionController =
      ActionController(name: '_SimulatorViewControllerBase');

  @override
  dynamic handleAccept() {
    final _$actionInfo = _$_SimulatorViewControllerBaseActionController
        .startAction(name: '_SimulatorViewControllerBase.handleAccept');
    try {
      return super.handleAccept();
    } finally {
      _$_SimulatorViewControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic handleRejected() {
    final _$actionInfo = _$_SimulatorViewControllerBaseActionController
        .startAction(name: '_SimulatorViewControllerBase.handleRejected');
    try {
      return super.handleRejected();
    } finally {
      _$_SimulatorViewControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
finalRateCredit: ${finalRateCredit},
finalRateDebit: ${finalRateDebit},
competitorDebit: ${competitorDebit},
competitorCredit: ${competitorCredit},
isValidDebit: ${isValidDebit},
isValidCredit: ${isValidCredit},
isAcceptableRate: ${isAcceptableRate},
minimumDebit: ${minimumDebit},
minimumCredit: ${minimumCredit}
    ''';
  }
}
