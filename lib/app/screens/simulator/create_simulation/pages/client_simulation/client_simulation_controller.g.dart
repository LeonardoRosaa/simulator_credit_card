// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client_simulation_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ClientSimulationController on _ClientSimulationControllerBase, Store {
  final _$cpfAtom = Atom(name: '_ClientSimulationControllerBase.cpf');

  @override
  String get cpf {
    _$cpfAtom.reportRead();
    return super.cpf;
  }

  @override
  set cpf(String value) {
    _$cpfAtom.reportWrite(value, super.cpf, () {
      super.cpf = value;
    });
  }

  final _$clientAtom = Atom(name: '_ClientSimulationControllerBase.client');

  @override
  ClientModel get client {
    _$clientAtom.reportRead();
    return super.client;
  }

  @override
  set client(ClientModel value) {
    _$clientAtom.reportWrite(value, super.client, () {
      super.client = value;
    });
  }

  final _$_ClientSimulationControllerBaseActionController =
      ActionController(name: '_ClientSimulationControllerBase');

  @override
  dynamic changeCpf(String newCpf) {
    final _$actionInfo = _$_ClientSimulationControllerBaseActionController
        .startAction(name: '_ClientSimulationControllerBase.changeCpf');
    try {
      return super.changeCpf(newCpf);
    } finally {
      _$_ClientSimulationControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeEmail(String newEmail) {
    final _$actionInfo = _$_ClientSimulationControllerBaseActionController
        .startAction(name: '_ClientSimulationControllerBase.changeEmail');
    try {
      return super.changeEmail(newEmail);
    } finally {
      _$_ClientSimulationControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changePhone(String newPhone) {
    final _$actionInfo = _$_ClientSimulationControllerBaseActionController
        .startAction(name: '_ClientSimulationControllerBase.changePhone');
    try {
      return super.changePhone(newPhone);
    } finally {
      _$_ClientSimulationControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
cpf: ${cpf},
client: ${client}
    ''';
  }
}
