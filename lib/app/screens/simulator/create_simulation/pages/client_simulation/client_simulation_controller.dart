import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:simulator_credit_card/app/models/client_model.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/stores/create_simulator_store.dart';

part 'client_simulation_controller.g.dart';

class ClientSimulationController = _ClientSimulationControllerBase
    with _$ClientSimulationController;

abstract class _ClientSimulationControllerBase with Store {

  CreateSimulatorStore _createSimulatorStore;

  _ClientSimulationControllerBase(this._createSimulatorStore);

  final formKey = GlobalKey<FormState>();

  @observable
  String cpf = '';

  @observable
  ClientModel client = new ClientModel();

  @action
  changeCpf(String newCpf) {
    client.cpf = newCpf;
    client = client;
  }

  @action 
  changeEmail(String newEmail) {
    client.email = newEmail;
    client = client;
  }

  @action
  changePhone(String newPhone) {
    client.phone = newPhone;
    client = client;
  }

  handleNextStep() {
    _createSimulatorStore.changeClient(client);

    if (formKey.currentState.validate()) {
      Modular.to.pushNamed('/simulator/store/client/branch');
    }
  }

  String validatorField(String value) {
    if (value == null || value.isEmpty) {
      return 'Campo obrigatório';
    }

    return null;
  }
}
