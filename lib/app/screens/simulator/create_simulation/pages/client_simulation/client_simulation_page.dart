import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/shared/widgets/next_button_widget.dart';
import 'package:simulator_credit_card/app/shared/widgets/input/input_widget.dart';
import 'package:simulator_credit_card/app/utils/patterns_utils.dart';
import 'client_simulation_controller.dart';

class ClientSimulationPage extends StatefulWidget {
  final String title;
  const ClientSimulationPage({Key key, this.title = "ClientSimulation"})
      : super(key: key);

  @override
  _ClientSimulationPageState createState() => _ClientSimulationPageState();
}

class _ClientSimulationPageState
    extends ModularState<ClientSimulationPage, ClientSimulationController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        elevation: 0,
        title: Text('Dados cliente'),
        backgroundColor: Color.fromRGBO(157, 173, 245, 1),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Form(
                  key: controller.formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),
                      Image(
                        image: AssetImage('assets/person.png'), 
                        width: 100
                      ),
                      SizedBox(height: 20),
                      InputWidget(
                        hintText: 'CPF', 
                        onChange: controller.changeCpf, 
                        validator: controller.validatorField,
                        inputFormatters: [PatternsUtils.cpf]
                      ),
                      SizedBox(height: 10),
                      InputWidget(
                        hintText: 'Telefone', 
                        validator: controller.validatorField,
                        inputFormatters: [PatternsUtils.phone],
                        onChange: controller.changePhone,
                      ),
                      SizedBox(height: 10),
                      InputWidget(
                        hintText: 'Email',
                        onChange: controller.changeEmail
                      ),
                      SizedBox(height: 20),
                      NextButtonWidget(onTap: controller.handleNextStep)
                     
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
