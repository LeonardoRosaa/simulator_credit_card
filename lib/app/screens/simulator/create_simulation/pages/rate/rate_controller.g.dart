// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rate_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RateController on _RateControllerBase, Store {
  final _$debitAtom = Atom(name: '_RateControllerBase.debit');

  @override
  RateModel get debit {
    _$debitAtom.reportRead();
    return super.debit;
  }

  @override
  set debit(RateModel value) {
    _$debitAtom.reportWrite(value, super.debit, () {
      super.debit = value;
    });
  }

  final _$creditAtom = Atom(name: '_RateControllerBase.credit');

  @override
  RateModel get credit {
    _$creditAtom.reportRead();
    return super.credit;
  }

  @override
  set credit(RateModel value) {
    _$creditAtom.reportWrite(value, super.credit, () {
      super.credit = value;
    });
  }

  final _$_RateControllerBaseActionController =
      ActionController(name: '_RateControllerBase');

  @override
  dynamic changeDebitRateCompetitor(String newRate) {
    final _$actionInfo = _$_RateControllerBaseActionController.startAction(
        name: '_RateControllerBase.changeDebitRateCompetitor');
    try {
      return super.changeDebitRateCompetitor(newRate);
    } finally {
      _$_RateControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeDebitMinimumDiscount(String newDiscount) {
    final _$actionInfo = _$_RateControllerBaseActionController.startAction(
        name: '_RateControllerBase.changeDebitMinimumDiscount');
    try {
      return super.changeDebitMinimumDiscount(newDiscount);
    } finally {
      _$_RateControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeCreditRateCompetitor(String newRate) {
    final _$actionInfo = _$_RateControllerBaseActionController.startAction(
        name: '_RateControllerBase.changeCreditRateCompetitor');
    try {
      return super.changeCreditRateCompetitor(newRate);
    } finally {
      _$_RateControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeCreditMinimumDiscount(String newDiscount) {
    final _$actionInfo = _$_RateControllerBaseActionController.startAction(
        name: '_RateControllerBase.changeCreditMinimumDiscount');
    try {
      return super.changeCreditMinimumDiscount(newDiscount);
    } finally {
      _$_RateControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
debit: ${debit},
credit: ${credit}
    ''';
  }
}
