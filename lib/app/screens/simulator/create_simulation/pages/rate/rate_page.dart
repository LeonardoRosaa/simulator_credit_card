import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/shared/widgets/next_button_widget.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/shared/widgets/text_title_widget.dart';
import 'package:simulator_credit_card/app/shared/widgets/input/input_widget.dart';
import 'rate_controller.dart';

class RatePage extends StatefulWidget {
  final String title;
  const RatePage({Key key, this.title = "Rate"}) : super(key: key);

  @override
  _RatePageState createState() => _RatePageState();
}

class _RatePageState extends ModularState<RatePage, RateController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Concorrente'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(10),
            child: Form(
              key: controller.formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  TextTitleWidget(text: 'Débito'),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Flexible(
                        child: InputWidget(
                          onChange: controller.changeDebitRateCompetitor,
                          hintText: 'Taxa %',
                          validator: controller.validatorField,
                          textInputType: TextInputType.number,
                        )
                      ),
                      SizedBox(width: 20),
                      Flexible(
                        child: InputWidget(
                          onChange: controller.changeDebitMinimumDiscount,
                          hintText: 'Desconto %', 
                          validator: controller.validatorField,
                          textInputType: TextInputType.number
                        )
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  TextTitleWidget(text: 'Crédito'),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Flexible(
                        child: InputWidget(
                          onChange: controller.changeCreditRateCompetitor,
                          hintText: 'Taxa %',
                          validator: controller.validatorField,
                          textInputType: TextInputType.number,
                        )
                      ),
                      SizedBox(width: 20),
                      Flexible(
                        child: InputWidget(
                          onChange: controller.changeCreditMinimumDiscount,
                          hintText: 'Desconto %', 
                          validator: controller.validatorField,
                          textInputType: TextInputType.number
                        )
                      )
                    ],
                  ),
                  SizedBox(height: 20),
                  NextButtonWidget(onTap: controller.handleBuildSimulation, text: 'Simular',)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
