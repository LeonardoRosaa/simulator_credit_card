import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:simulator_credit_card/app/models/rate_model.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/stores/create_simulator_store.dart';

part 'rate_controller.g.dart';

class RateController = _RateControllerBase with _$RateController;

abstract class _RateControllerBase with Store {
  CreateSimulatorStore _createSimulatorStore;

  _RateControllerBase(this._createSimulatorStore);

  final formKey = GlobalKey<FormState>();

  @observable
  RateModel debit = new RateModel();

  @observable
  RateModel credit = new RateModel();

  @action
  changeDebitRateCompetitor(String newRate) {
    debit.rateCompetitor = double.parse(newRate);
    debit = debit;
  }

  @action
  changeDebitMinimumDiscount(String newDiscount) {
    debit.minimumDiscount = double.parse(newDiscount);
    debit = debit;
  }

   @action
  changeCreditRateCompetitor(String newRate) {
    credit.rateCompetitor = double.parse(newRate);
    credit = credit;
  }

  @action
  changeCreditMinimumDiscount(String newDiscount) {
    credit.minimumDiscount = double.parse(newDiscount);
    credit = credit;
  }

  handleBuildSimulation() {
    if (formKey.currentState.validate()) {
      _createSimulatorStore.changeCreditRate(credit);
      _createSimulatorStore.changeDebitRate(debit);

    Modular.to.pushNamed('/simulator/store/client/simulator');
    }
  }

  String validatorField(String value) {
    if (value == null || value.isEmpty) {
      return 'Campo obrigatório';
    }

    return null;
  }
}
