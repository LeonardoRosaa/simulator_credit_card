// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client_branch_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ClientBranchController on _ClientBranchControllerBase, Store {
  Computed<int> _$branchesLengthComputed;

  @override
  int get branchesLength =>
      (_$branchesLengthComputed ??= Computed<int>(() => super.branchesLength,
              name: '_ClientBranchControllerBase.branchesLength'))
          .value;

  final _$branchesAtom = Atom(name: '_ClientBranchControllerBase.branches');

  @override
  ObservableFuture<List<BranchModel>> get branches {
    _$branchesAtom.reportRead();
    return super.branches;
  }

  @override
  set branches(ObservableFuture<List<BranchModel>> value) {
    _$branchesAtom.reportWrite(value, super.branches, () {
      super.branches = value;
    });
  }

  final _$_ClientBranchControllerBaseActionController =
      ActionController(name: '_ClientBranchControllerBase');

  @override
  dynamic findAllBranches() {
    final _$actionInfo = _$_ClientBranchControllerBaseActionController
        .startAction(name: '_ClientBranchControllerBase.findAllBranches');
    try {
      return super.findAllBranches();
    } finally {
      _$_ClientBranchControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
branches: ${branches},
branchesLength: ${branchesLength}
    ''';
  }
}
