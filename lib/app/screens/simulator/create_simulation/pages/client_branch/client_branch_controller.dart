import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:simulator_credit_card/app/interfaces/branch_repository_interface.dart';
import 'package:simulator_credit_card/app/models/branch_model.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/stores/create_simulator_store.dart';

part 'client_branch_controller.g.dart';

class ClientBranchController = _ClientBranchControllerBase
    with _$ClientBranchController;

abstract class _ClientBranchControllerBase with Store {
  
  CreateSimulatorStore _createSimulatorStore;
  IBranchRepository _branchRepository;

  _ClientBranchControllerBase(this._createSimulatorStore, this._branchRepository) {
    this.findAllBranches();
  }

  @observable
  ObservableFuture<List<BranchModel>> branches;

  @computed
  int get branchesLength {
    return this.branches?.value == null ? 0 : this.branches?.value?.length;
  }

  @action
  findAllBranches() {
    this.branches = this._branchRepository.findAll().asObservable();
  }

  handleSelectBrach(String branchId) {
    var branch = branches.value.firstWhere((branch) => branch.id == branchId);
    _createSimulatorStore.changeBranchClient(branch);

    Modular.to.pushNamed('/simulator/store/client/competitors');
  }

}
