import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'client_branch_controller.dart';

class ClientBranchPage extends StatefulWidget {
  final String title;
  const ClientBranchPage({Key key, this.title = "ClientBranch"})
      : super(key: key);

  @override
  _ClientBranchPageState createState() => _ClientBranchPageState();
}

class _ClientBranchPageState
    extends ModularState<ClientBranchPage, ClientBranchController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(157, 173, 245, 1),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text('Qual o ramo de atividade?'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Observer(
           builder: (_) => ListView.builder(
             itemCount: controller.branchesLength,
             itemBuilder: (_, index) {
               var branch = controller.branches.value[index];

               return InkWell(
                 onTap: () => {
                   controller.handleSelectBrach(branch?.id)
                 },
                 highlightColor: Colors.transparent,
                 splashColor: Colors.transparent,
                 child: Container(
                   width: double.infinity,
                   margin: EdgeInsets.symmetric(vertical: 10),
                   padding: EdgeInsets.symmetric(vertical: 10),
                   decoration: BoxDecoration(
                     border: Border(bottom: BorderSide(
                       color: Colors.white
                     ))
                   ),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: <Widget>[
                       Icon(
                         Icons.fiber_smart_record,
                         color: Colors.white,
                         size: 28,
                       ),
                       Text(
                         '${branch?.name}',
                         textAlign: TextAlign.center,
                         style: TextStyle(
                           fontSize: 18,
                           fontWeight: FontWeight.bold,
                           color: Colors.white
                         ),
                       ),
                     ],
                   ),
                 ),
               );
             },

          ),
        ),
      ),
    );
  }
}
