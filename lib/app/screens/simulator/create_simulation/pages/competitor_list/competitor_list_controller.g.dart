// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'competitor_list_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CompetitorListController on _CompetitorListControllerBase, Store {
  Computed<int> _$competitorsLengthComputed;

  @override
  int get competitorsLength => (_$competitorsLengthComputed ??= Computed<int>(
          () => super.competitorsLength,
          name: '_CompetitorListControllerBase.competitorsLength'))
      .value;

  final _$competitorsAtom =
      Atom(name: '_CompetitorListControllerBase.competitors');

  @override
  ObservableFuture<List<CompetitorModel>> get competitors {
    _$competitorsAtom.reportRead();
    return super.competitors;
  }

  @override
  set competitors(ObservableFuture<List<CompetitorModel>> value) {
    _$competitorsAtom.reportWrite(value, super.competitors, () {
      super.competitors = value;
    });
  }

  final _$_CompetitorListControllerBaseActionController =
      ActionController(name: '_CompetitorListControllerBase');

  @override
  dynamic findAllCompetitor() {
    final _$actionInfo = _$_CompetitorListControllerBaseActionController
        .startAction(name: '_CompetitorListControllerBase.findAllCompetitor');
    try {
      return super.findAllCompetitor();
    } finally {
      _$_CompetitorListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
competitors: ${competitors},
competitorsLength: ${competitorsLength}
    ''';
  }
}
