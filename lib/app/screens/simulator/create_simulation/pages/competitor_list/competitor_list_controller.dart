import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:simulator_credit_card/app/interfaces/competitors_repository_interface.dart';
import 'package:simulator_credit_card/app/models/competitor_model.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/stores/create_simulator_store.dart';

part 'competitor_list_controller.g.dart';

class CompetitorListController = _CompetitorListControllerBase
    with _$CompetitorListController;

abstract class _CompetitorListControllerBase with Store {

  CreateSimulatorStore _createSimulatorStore;
  ICompetitorsRepository _competitorsRepository;

  @observable
  ObservableFuture<List<CompetitorModel>> competitors;

  _CompetitorListControllerBase(this._createSimulatorStore, this._competitorsRepository) {
    findAllCompetitor();
  }

  @computed
  int get competitorsLength {
    return this.competitors?.value == null ? 0 : this.competitors?.value?.length;
  }

  @action
  findAllCompetitor() {
    this.competitors = _competitorsRepository.findAll().asObservable();
  }

  handleSelectCompetitor(String competitorId) {
    var competitor = competitors.value.firstWhere((competitor) => competitor.id == competitorId);
    _createSimulatorStore.changeCompetitor(competitor);

    Modular.to.pushNamed('/simulator/store/client/rate');
  }
}
