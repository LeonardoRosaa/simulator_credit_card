import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'competitor_list_controller.dart';

class CompetitorListPage extends StatefulWidget {
  final String title;
  const CompetitorListPage({Key key, this.title = "CompetitorList"})
      : super(key: key);

  @override
  _CompetitorListPageState createState() => _CompetitorListPageState();
}

class _CompetitorListPageState
    extends ModularState<CompetitorListPage, CompetitorListController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(157, 173, 245, 1),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text('Qual é o concorrente?'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Observer(
          builder: (_) => ListView.builder(
            itemCount: controller.competitorsLength,
            itemBuilder: (_, index) {
              var competitor = controller.competitors.value[index];

              return InkWell(
                onTap: () => {
                  controller.handleSelectCompetitor(competitor?.id)
                },
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                child: Container(
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(vertical: 10),
                  padding: EdgeInsets.symmetric(vertical: 10),
                  decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(
                      color: Colors.white
                    ))
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Icon(
                        Icons.scatter_plot,
                        color: Colors.white,
                        size: 28,
                      ),
                      Text(
                        '${competitor?.name}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },

          ),
        ),
      ),
    );
  }
}
