
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:simulator_credit_card/app/interfaces/branch_repository_interface.dart';
import 'package:simulator_credit_card/app/interfaces/competitors_repository_interface.dart';
import 'package:simulator_credit_card/app/interfaces/simulator_repository_interface.dart';
import 'package:simulator_credit_card/app/repositories/branch_repository.dart';
import 'package:simulator_credit_card/app/repositories/competitors_repository.dart';
import 'package:simulator_credit_card/app/repositories/simulator_repository.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/pages/client_branch/client_branch_controller.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/pages/client_simulation/client_simulation_controller.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/pages/competitor_list/competitor_list_controller.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/pages/competitor_list/competitor_list_page.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/pages/rate/rate_controller.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/pages/rate/rate_page.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/pages/simulator_view/simulator_view_controller.dart';
import 'package:simulator_credit_card/app/screens/simulator/create_simulation/stores/create_simulator_store.dart';

import 'pages/client_branch/client_branch_page.dart';
import 'pages/client_simulation/client_simulation_page.dart';
import 'pages/simulator_view/simulator_view_page.dart';

class CreateSimulationModule extends ChildModule {
  @override
  List<Bind> get binds => [
      Bind((i) => SimulatorViewController(i.get<CreateSimulatorStore>(), i.get<ISimulatorRepository>()), singleton: false),
        Bind((i) => RateController(i.get<CreateSimulatorStore>())),
        Bind((i) => CompetitorListController(i.get<CreateSimulatorStore>(), i.get<CompetitorsRepository>())),
        Bind((i) => ClientBranchController(i.get<CreateSimulatorStore>(), i.get<IBranchRepository>())),
        Bind((i) => ClientSimulationController(i.get<CreateSimulatorStore>())),
        Bind<ICompetitorsRepository>((i) => CompetitorsRepository(firestore: Firestore.instance)),
        Bind<IBranchRepository>((i) => BranchRepository(firestore: Firestore.instance)),
        Bind<ISimulatorRepository>((i) => SimulatorRepository(firestore: Firestore.instance)),
        // stores
        Bind((i) => CreateSimulatorStore())
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute,
            child: (_, args) => ClientSimulationPage()),
        Router('/branch', child: (_, args) => ClientBranchPage()),
        Router('/competitors', child: (_, args) => CompetitorListPage()),
        Router('/rate', child: (_, args) => RatePage()),
        Router('/simulator', child: (_, args) => SimulatorViewPage())
      ];

  static Inject get to => Inject<CreateSimulationModule>.of();
}
