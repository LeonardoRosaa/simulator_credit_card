import 'package:flutter/material.dart';

class TextTableRow extends StatelessWidget {
  final String text;
  final Color color;

  TextTableRow({
    @required this.text,
    this.color = Colors.black
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        '$text',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: color
        ),
      ),
    );
  }
}