import 'package:flutter/material.dart';

class TextTitleWidget extends StatelessWidget {

  final String text;

  TextTitleWidget({this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Text(
        '$text',
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
          color: Color.fromRGBO(0, 0, 0, .6)
        ),
      ),
    );
  }
}