import 'package:flutter/material.dart';

class SnackBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SnackBar(
      duration: Duration(days: 365),
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      ),
      content: Container(
        height: 300,
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
              height: 2,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.black45,
                borderRadius: BorderRadius.all(Radius.circular(40))
              ),
            ),
            Icon(
              Icons.done_all,
              size: 140,
              color: Colors.greenAccent,
            ),
            Text(
              'Salvo com sucesso',
              style: TextStyle(
                fontSize: 23,
                color: Colors.black45
              ),
            )
          ],
        ),
      ),
    );
  }
}