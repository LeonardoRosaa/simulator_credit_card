import 'package:flutter/material.dart';

class NextButtonWidget extends StatelessWidget {

  final Function onTap;
  final String text;
  final IconData icon;
  final List<Color> colorsGradient; 

  final List<Color> colors = [Color.fromRGBO(181, 196, 255, 1), Color.fromRGBO(157, 173, 245, 1)];
  
  NextButtonWidget({
    @required this.onTap,
    this.text = 'Próximo',
    this.icon = Icons.arrow_forward_ios,
    this.colorsGradient
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: 58,
          decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            stops: [
              .1,
              1
            ],
            colors: colorsGradient != null ? colorsGradient : colors
          )
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                '$text',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 17
                ),
              ),
              Icon(
                icon,
                color: Colors.white,
              )
            ],
          ),
        ),
      )
    ),
  );
}
}