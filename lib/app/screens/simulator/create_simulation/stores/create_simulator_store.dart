import 'package:mobx/mobx.dart';
import 'package:simulator_credit_card/app/models/branch_model.dart';
import 'package:simulator_credit_card/app/models/client_model.dart';
import 'package:simulator_credit_card/app/models/competitor_model.dart';
import 'package:simulator_credit_card/app/models/rate_model.dart';
import 'package:simulator_credit_card/app/models/simulator_data_model.dart';

part 'create_simulator_store.g.dart';

class CreateSimulatorStore = _CreateSimulatorStoreBase
    with _$CreateSimulatorStore;

abstract class _CreateSimulatorStoreBase with Store {

  @observable
  SimulatorDataModel simulator = new SimulatorDataModel();

  @action
  changeClient(ClientModel newClient) {
    simulator.client = newClient;
    simulator = simulator;
  }

  @action
  changeCompetitor(CompetitorModel newCompetitor) {
    simulator.competitor = newCompetitor;
    simulator = simulator;
  }

  @action
  changeBranchClient(BranchModel newBranch) {
    simulator.client.branch = newBranch;
    simulator = simulator;
  }

  @action
  changeDebitRate(RateModel newRate) {
    simulator.debitRate = newRate;
    simulator = simulator;
  }

  @action
  changeCreditRate(RateModel newRate) {
    simulator.creditRate = newRate;
    simulator = simulator;
  } 
  
}