// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_simulator_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CreateSimulatorStore on _CreateSimulatorStoreBase, Store {
  final _$simulatorAtom = Atom(name: '_CreateSimulatorStoreBase.simulator');

  @override
  SimulatorDataModel get simulator {
    _$simulatorAtom.reportRead();
    return super.simulator;
  }

  @override
  set simulator(SimulatorDataModel value) {
    _$simulatorAtom.reportWrite(value, super.simulator, () {
      super.simulator = value;
    });
  }

  final _$_CreateSimulatorStoreBaseActionController =
      ActionController(name: '_CreateSimulatorStoreBase');

  @override
  dynamic changeClient(ClientModel newClient) {
    final _$actionInfo = _$_CreateSimulatorStoreBaseActionController
        .startAction(name: '_CreateSimulatorStoreBase.changeClient');
    try {
      return super.changeClient(newClient);
    } finally {
      _$_CreateSimulatorStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeCompetitor(CompetitorModel newCompetitor) {
    final _$actionInfo = _$_CreateSimulatorStoreBaseActionController
        .startAction(name: '_CreateSimulatorStoreBase.changeCompetitor');
    try {
      return super.changeCompetitor(newCompetitor);
    } finally {
      _$_CreateSimulatorStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeBranchClient(BranchModel newBranch) {
    final _$actionInfo = _$_CreateSimulatorStoreBaseActionController
        .startAction(name: '_CreateSimulatorStoreBase.changeBranchClient');
    try {
      return super.changeBranchClient(newBranch);
    } finally {
      _$_CreateSimulatorStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeDebitRate(RateModel newRate) {
    final _$actionInfo = _$_CreateSimulatorStoreBaseActionController
        .startAction(name: '_CreateSimulatorStoreBase.changeDebitRate');
    try {
      return super.changeDebitRate(newRate);
    } finally {
      _$_CreateSimulatorStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeCreditRate(RateModel newRate) {
    final _$actionInfo = _$_CreateSimulatorStoreBaseActionController
        .startAction(name: '_CreateSimulatorStoreBase.changeCreditRate');
    try {
      return super.changeCreditRate(newRate);
    } finally {
      _$_CreateSimulatorStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
simulator: ${simulator}
    ''';
  }
}
