// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_simulator_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeSimulatorController on _HomeSimulatorControllerBase, Store {
  Computed<int> _$simulationLengthComputed;

  @override
  int get simulationLength => (_$simulationLengthComputed ??= Computed<int>(
          () => super.simulationLength,
          name: '_HomeSimulatorControllerBase.simulationLength'))
      .value;

  final _$simulationsAtom =
      Atom(name: '_HomeSimulatorControllerBase.simulations');

  @override
  ObservableFuture<List<SimulatorDataModel>> get simulations {
    _$simulationsAtom.reportRead();
    return super.simulations;
  }

  @override
  set simulations(ObservableFuture<List<SimulatorDataModel>> value) {
    _$simulationsAtom.reportWrite(value, super.simulations, () {
      super.simulations = value;
    });
  }

  final _$_HomeSimulatorControllerBaseActionController =
      ActionController(name: '_HomeSimulatorControllerBase');

  @override
  dynamic findAcceptSimulations() {
    final _$actionInfo =
        _$_HomeSimulatorControllerBaseActionController.startAction(
            name: '_HomeSimulatorControllerBase.findAcceptSimulations');
    try {
      return super.findAcceptSimulations();
    } finally {
      _$_HomeSimulatorControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
simulations: ${simulations},
simulationLength: ${simulationLength}
    ''';
  }
}
