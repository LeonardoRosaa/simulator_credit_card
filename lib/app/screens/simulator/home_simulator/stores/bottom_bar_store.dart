import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:simulator_credit_card/app/interfaces/csv_simulators_interface.dart';

part 'bottom_bar_store.g.dart';

class BottomBarStore = _BottomBarStoreBase
    with _$BottomBarStore;

abstract class _BottomBarStoreBase with Store {
  
  ICsvSimulators _csvSimulatorsService;

  _BottomBarStoreBase(this._csvSimulatorsService);

  handleClickNewSimulation() {
    Modular.to.pushNamed('/simulator/store/client');
  }

  Future<void> handleGenerateCSVSimulationsAccept() async {
    await _csvSimulatorsService.generateCSVSimulatorAcceptAndOpen();
  }
}