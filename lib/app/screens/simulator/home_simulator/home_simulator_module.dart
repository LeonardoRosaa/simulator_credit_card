import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:simulator_credit_card/app/interfaces/csv_simulators_interface.dart';
import 'package:simulator_credit_card/app/interfaces/simulator_repository_interface.dart';
import 'package:simulator_credit_card/app/repositories/simulator_repository.dart';
import 'package:simulator_credit_card/app/screens/simulator/home_simulator/stores/bottom_bar_store.dart';
import 'package:simulator_credit_card/app/services/csv_simulators_service.dart';
import 'home_simulator_controller.dart';
import 'home_simulator_page.dart';

class HomeSimulatorModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeSimulatorController(i.get<ISimulatorRepository>()), singleton: false),
        Bind((i) => BottomBarStore(i.get<ICsvSimulators>())),
        Bind<ICsvSimulators>((i) => CsvSimulatorsService(simulatorRepository: i.get<ISimulatorRepository>())),
        Bind<ISimulatorRepository>((i) => SimulatorRepository(firestore: Firestore.instance)),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => HomeSimulatorPage()),
      ];

  static Inject get to => Inject<HomeSimulatorModule>.of();
}
