import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:simulator_credit_card/app/constants/status_simulator_constant.dart';
import 'package:simulator_credit_card/app/interfaces/simulator_repository_interface.dart';
import 'package:simulator_credit_card/app/models/simulator_data_model.dart';

part 'home_simulator_controller.g.dart';

class HomeSimulatorController = _HomeSimulatorControllerBase
    with _$HomeSimulatorController;

abstract class _HomeSimulatorControllerBase with Store {
  
  ISimulatorRepository _simulatorRepository;

  _HomeSimulatorControllerBase(this._simulatorRepository) {
    findAcceptSimulations();
  }

  @observable
  ObservableFuture<List<SimulatorDataModel>> simulations;

  @computed
  int get simulationLength {
    return this.simulations?.value == null ? 0 : this.simulations?.value?.length;
  }

  @action
  findAcceptSimulations() {
    this.simulations = _simulatorRepository.findByStatus(StatusSimulator.accept).asObservable();
  }

  handleClickNewSimulation() {
    Modular.to.pushNamed('/simulator/store/client');
  }


}
