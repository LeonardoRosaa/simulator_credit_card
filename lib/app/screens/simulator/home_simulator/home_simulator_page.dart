import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:simulator_credit_card/app/screens/simulator/home_simulator/shared/widgets/bottom_bar_widget.dart';
import 'package:simulator_credit_card/app/screens/simulator/home_simulator/shared/widgets/card_widget.dart';
import 'home_simulator_controller.dart';

class HomeSimulatorPage extends StatefulWidget {
  final String title;
  const HomeSimulatorPage({Key key, this.title = "HomeSimulator"})
      : super(key: key);

  @override
  _HomeSimulatorPageState createState() => _HomeSimulatorPageState();
}

class _HomeSimulatorPageState
    extends ModularState<HomeSimulatorPage, HomeSimulatorController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomBarWidget(),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: FutureBuilder(
          future: controller.simulations,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return Observer(
                builder: (_) => Container( 
                  child: ListView.builder(
                    itemCount: controller.simulationLength,
                    itemBuilder: (_, index) => CardWidget(simulator: controller.simulations.value[index])),
                ),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      )
    );
  }
}
