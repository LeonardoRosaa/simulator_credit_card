import 'package:flutter/material.dart';

class ColumnCardWidget extends StatelessWidget {

  final String textTitle;
  final String textSubtitle;

  ColumnCardWidget({this.textTitle, this.textSubtitle});

  @override
  Widget build(BuildContext context) {
    return Flexible(
      fit: FlexFit.tight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('$textTitle', style: TextStyle(fontWeight: FontWeight.w300)),
          SizedBox(height: 3),
          Text('$textSubtitle',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.black87,
                  fontSize: 17))
        ],
      ),
    );
  }
}
