import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:simulator_credit_card/app/screens/simulator/home_simulator/stores/bottom_bar_store.dart';

class BottomBarWidget extends StatefulWidget {
  @override
  _BottomBarWidgetState createState() => _BottomBarWidgetState();
}

class _BottomBarWidgetState extends ModularState<BottomBarWidget, BottomBarStore> {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: Colors.transparent,
      elevation: 0,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            InkWell(
              onTap: null,
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              child: Icon(
                Icons.home,
                color: Color.fromRGBO(44, 44, 220, 1),
                size: 30,
              ),
            ),
            InkWell(
              onTap: controller.handleClickNewSimulation,
              child: Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(bottom: 5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Color.fromRGBO(44, 44, 220, 1)
                ),
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 27,
                ),
              ),
            ),
            InkWell(
              onTap: controller.handleGenerateCSVSimulationsAccept,
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              child: Icon(
                Icons.trending_down,
                color: Color.fromRGBO(181, 196, 255, 1),
                size: 30,
              ),
            ),
          ],
        ),
      ),
    );
  }
}