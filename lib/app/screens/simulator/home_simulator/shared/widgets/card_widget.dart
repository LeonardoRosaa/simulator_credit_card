import 'package:flutter/material.dart';
import 'package:simulator_credit_card/app/constants/status_simulator_constant.dart';
import 'package:simulator_credit_card/app/models/simulator_data_model.dart';
import 'package:simulator_credit_card/app/screens/simulator/home_simulator/shared/widgets/column_card_widget.dart';
import 'package:simulator_credit_card/app/utils/rates.dart';

class CardWidget extends StatelessWidget {

  final SimulatorDataModel simulator;

  CardWidget({
    @required this.simulator
  });

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.horizontal,
       children: [
         Expanded(
          child: Container(
          margin: EdgeInsets.symmetric(vertical: 5),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(232, 234, 237, 1),
                        border: Border(
                          bottom: BorderSide(
                            width: 1,
                            color: Color.fromRGBO(44, 44, 220, 1),
                          )
                        )
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('${simulator?.client?.phone}'),
                              Text('${simulator?.client?.cpf}'),
                            ],
                          ),
                          SizedBox(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              
                              Text('${simulator?.client?.branch?.name}'),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10))
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    ColumnCardWidget(
                      textTitle: 'Crédito',
                      textSubtitle: '${Rates.instance.calcRate(simulator.creditRate.rateCompetitor, simulator.creditRate.minimumDiscount).toStringAsFixed(2)}%',
                    ),
                    ColumnCardWidget(
                      textTitle: 'Débito',
                      textSubtitle: '${Rates.instance.calcRate(simulator.debitRate.rateCompetitor, simulator.debitRate.minimumDiscount).toStringAsFixed(2)}%'
                    ),
                    ColumnCardWidget(
                      textTitle: 'Status',
                      textSubtitle: simulator.status == StatusSimulator.accept ? 'Aprovado' : 'Reijeitado',
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      )],
    );
  }
}