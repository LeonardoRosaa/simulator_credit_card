import 'package:flutter/material.dart';
import 'package:simulator_credit_card/app/app_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

void main() => runApp(ModularApp(module: AppModule()));
