import 'package:flutter_test/flutter_test.dart';
import 'package:simulator_credit_card/app/utils/rates.dart';

void main() {

  group('Rate class', () {
    test('value should return 100', () {
      double result = Rates.instance.multiplyPerOneHundred(0.6);

      expect(result, 60);
    });

    test('should returns 4.05', () {
      double result = Rates.instance.calcRate(4.5, 10);

      expect(result, 4.05);
    });

  });
}